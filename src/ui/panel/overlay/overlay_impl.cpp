#include "ftxui/component/component.hpp"
#include "ui/panel/panel.hpp"
#include "utils.hpp"

#include <array>
#include <cstdio>
#include <filesystem>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

using namespace ftxui;

namespace ui {

namespace {

/* Handle overlay component with checkbox, title and dropbox */
class Overlay : public ComponentBase {
 public:
  Overlay(std::vector<std::string>* overlay,
          int pos,
          int* selected,
          bool* checked)
      : overlays_(overlay), pos_(pos), selected_(selected), checked_(checked) {
    Add(Container::Horizontal({
        checkBox_,
        dropDown_,
    }));
  }

  ~Overlay() = default;

 private:
  std::vector<std::string>* overlays_;
  int pos_;
  int* selected_;
  bool* checked_;
  Component dropDown_ = Dropdown(overlays_, selected_);
  Component checkBox_ =
      Checkbox("Overlay " + std::to_string(pos_) + " : ", checked_);

  Element Render() override {
    return hbox({
        checkBox_->Render() | vcenter,
        dropDown_->Render(),
    });
  }
};

class overlayImpl : public PanelBase {
 public:
  overlayImpl() {
    get_overlays();
    check_uEnv();
    build_UI();

    Add(Container::Vertical({
        container_,
        button_,
    }));
  }

  ~overlayImpl() = default;

  std::string Title() override { return "Overlay"; }

 private:
  /* Scan all the dtbo file in /boot/dtbs and store in vector */
  void get_overlays() {
    overlays_.clear();
    const std::string path_version = "/proc/version";
    std::string line;

    std::ifstream versionFile(path_version);
    versionFile >> line;
    versionFile >> line;
    versionFile >> kernel_version;
    versionFile.close();

    std::string overlay_path = "/boot/dtbs/" + kernel_version + "/overlays";

    overlays_.push_back("<fileX>.dtbo");

    for (const auto& file : std::filesystem::directory_iterator(overlay_path)) {
      std::stringstream ss(file.path());

      while (getline(ss, line, '/'))
        ;

      overlays_.push_back(line);
    }
  }

  /* Build Overlay and Tab Component */
  void build_UI() {
    // Create Overlay Component
    for (int i = 0; i < (int)drop_selected_.size(); i++) {
      container_->Add(
          Make<Overlay>(&overlays_, i, &drop_selected_[i], &box_checked_[i]));
    }

    // Create Tab Component
    tab_->Add(container_);
    tab_->Add(Container::Vertical({}));
  }

  /* Handle if overlay selected twice             */
  /* If overlay choosed it will set to empty file */
  /* If overlay file not selected can't enable it */
  void no_overlap() {
    for (int i = 0; i < (int)drop_selected_.size(); i++) {
      if (drop_selected_[i] == 0)
        box_checked_[i] = false;

      if (i == conSelected_)
        continue;

      if (drop_selected_[conSelected_] == drop_selected_[i]) {
        drop_selected_[conSelected_] = 0;
      }
    }
  }

  /* Read file uEnv and update in BB-Config */
  void check_uEnv() {
    std::ifstream infile;
    infile.open(File_uEnv);
    std::string lines;

    if (!infile) {
      tabSelected_ = 1;
    }

    while (getline(infile, lines)) {
      if (!lines.compare("###Overide capes with eeprom")) {
        for (int i = 0; i < 4; i++) {
          getline(infile, lines);
          std::stringstream ss(lines);
          std::string temp, dtbo;

          getline(ss, temp, '=');
          getline(ss, dtbo, '=');

          if (!temp.compare("#uboot_overlay_addr" + std::to_string(i))) {
            box_checked_[i] = false;

            for (int j = 0; j < (int)overlays_.size(); j++) {
              if (!overlays_[j].compare(dtbo)) {
                drop_selected_[i] = j;
              }
            }
          } else if (!temp.compare("uboot_overlay_addr" + std::to_string(i))) {
            box_checked_[i] = true;

            for (int j = 0; j < (int)overlays_.size(); j++) {
              if (!overlays_[j].compare(dtbo)) {
                drop_selected_[i] = j;
              }
            }

          } else {
            box_checked_[i] = false;
          }
        }
      }

      if (!lines.compare("###Additional custom capes")) {
        for (int i = 4; i < 8; i++) {
          getline(infile, lines);
          std::stringstream ss(lines);
          std::string temp, dtbo;

          getline(ss, temp, '=');
          getline(ss, dtbo, '=');

          if (!temp.compare("#uboot_overlay_addr" + std::to_string(i))) {
            box_checked_[i] = false;

            for (int j = 0; j < (int)overlays_.size(); j++) {
              if (!overlays_[j].compare(dtbo)) {
                drop_selected_[i] = j;
              }
            }
          } else if (!temp.compare("uboot_overlay_addr" + std::to_string(i))) {
            box_checked_[i] = true;

            for (int j = 0; j < (int)overlays_.size(); j++) {
              if (!overlays_[j].compare(dtbo)) {
                drop_selected_[i] = j;
              }
            }

          } else {
            box_checked_[i] = false;
          }
        }
      }
    }
    infile.close();
  }

  /* Apply changes by updating uEnv.txt file */
  void overwrite_uEnv() {
    std::ifstream infile;
    std::ofstream outfile;

    infile.open(File_uEnv);
    outfile.open(File_Backup);

    std::string lines;

    while (getline(infile, lines)) {
      if (!lines.compare("###Overide capes with eeprom")) {
        outfile << lines << std::endl;
        for (int i = 0; i < 4; i++) {
          getline(infile, lines);
          if (box_checked_[i])
            outfile << "uboot_overlay_addr" << i << "="
                    << overlays_[drop_selected_[i]] << std::endl;
          else
            outfile << "#uboot_overlay_addr" << i << "="
                    << overlays_[drop_selected_[i]] << std::endl;
        }
        continue;
      }

      if (!lines.compare("###Additional custom capes")) {
        outfile << lines << std::endl;
        for (int i = 4; i < 8; i++) {
          getline(infile, lines);
          if (box_checked_[i])
            outfile << "uboot_overlay_addr" << i << "="
                    << overlays_[drop_selected_[i]] << std::endl;
          else
            outfile << "#uboot_overlay_addr" << i << "="
                    << overlays_[drop_selected_[i]] << std::endl;
        }
        continue;
      }

      outfile << lines << std::endl;
    }

    infile.close();
    outfile.close();

    // Replace old file with new file
    std::rename(File_Backup.c_str(), File_uEnv.c_str());
  }

  Element Render() override {
    no_overlap();

    Element content = vbox({
        text("Kernel Version : " + kernel_version),
        text("Currently only support maximum 8 overlays") | color(Color::Red),
        separator(),
        container_->Render() | flex | frame | vscroll_indicator,
        separator(),
        button_->Render(),
    });

    Element error =
        vbox({text("Features not support!") | color(Color::Red) | bold});

    // Check if tabSelected == 0 return content page
    return (tabSelected_ == 0) ? content : error;
  }

  int conSelected_ = 0;
  int tabSelected_ = 0;
  std::string kernel_version;
  const std::string File_uEnv = "/boot/uEnv.txt";
  const std::string File_Backup = "./backup.txt";
  std::vector<std::string> overlays_;
  std::array<int, 8> drop_selected_ = {0, 0, 0, 0, 0, 0, 0, 0};
  std::array<bool, 8> box_checked_ = {false, false, false, false,
                                      false, false, false, false};
  Component tab_ = Container::Vertical({}, &tabSelected_);
  Component container_ = Container::Vertical({}, &conSelected_);
  Component button_ = Button("Apply", [this] { overwrite_uEnv(); });
};

}  // namespace

namespace panel {
Panel overlay() {
  return Make<overlayImpl>();
}

}  // namespace panel

}  // namespace ui