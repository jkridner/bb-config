#include <errno.h>
#include <gio/gio.h>
#include <linux/if.h>
#include <linux/wireless.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <unistd.h>
#include <algorithm>
#include <chrono>
#include <cstdio>
#include <ctime>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <thread>
#include <unordered_map>
#include <vector>
#include "ftxui/component/component.hpp"
#include "ftxui/dom/elements.hpp"
#include "ui/panel/panel.hpp"
#include "utils.hpp"

namespace ui {

namespace {

constexpr const char CONNMAN_SERVICE_F_PATH[]{"/var/lib/connman/"};

/* Definition of possible strings in the .config files */
constexpr const char SERVICE_KEY_TYPE[]{"Type"};
constexpr const char SERVICE_KEY_NAME[]{"Name"};
constexpr const char SERVICE_KEY_PASSPHRASE[]{"Passphrase"};

struct connman_data {
  /* wifi | ethernet */
  std::string type;

  /* SSID */
  std::string name;

  /* Password */
  std::string pass;
};

class WiFiImpl : public PanelBase {
 public:
  WiFiImpl(ScreenInteractive* screen) : screen_(screen) {
    // TODO:Is there a better method than this?
    if (!std::filesystem::exists("/sys/class/net/wlan0")) {
      wifiCompatiblity = false;
    }
    if (wifiCompatiblity) {
      wifi_toggle_ = Button("Toggle WiFi", [&] { ToggleWifi(); });
      scan_button = Button(&scan_label_, [&] { Scan(); });
      connect_button = Button("Connect", [&] { Connect(); });
      disconnect_button = Button("Disconnect", [&] { Disconnect(); });
      back_button = Button("Back", [&] { activity = ActivityMain; });
      pass_input = Input(&password, "password");
      option.on_enter = [&] { activity = ActivityConnect; };
      menu_scan = Menu(&wifi_list_, &selected, &option);

      Add(Container::Tab(
          {
              Container::Vertical({
                  Container::Horizontal({
                      wifi_toggle_,
                      scan_button,
                  }),
                  menu_scan,
              }),
              Container::Vertical({
                  pass_input,
                  Container::Horizontal({
                      connect_button,
                      disconnect_button,
                      back_button,
                  }),
              }),
          },
          &activity));

      // Populate the wifi network in the background.
      Scan();
    }
  };
  ~WiFiImpl() {
    if (wifiCompatiblity)
      if (wifi_scan_.joinable()) {
        wifi_scan_.join();
      }
  }
  std::string Title() override { return "WiFi"; }

  Element Render() override {
    if (wifiCompatiblity) {
      while (wifi_list_receiver_->HasPending())
        wifi_list_receiver_->Receive(&wifi_list_);

      if (activity == ActivityMain)
        return RenderMain();

      if (activity == ActivityConnect)
        return RenderConnect();

      return text("Not implemented");
    } else {
      return text("Feature not supported");
    }
  }

  Element RenderMain() {
    scan_label_ =
        scanning_ ? "Scan network (Status: scanning...)" : "Scan network";
    auto wifi_status_ =
        (WifiStatus() ? "WiFi Status : Enabled" : "WiFi Status : Disabled");
    auto current_network = "Current Network: " + ActiveWifiName();

    return vbox({
               text(wifi_status_),
               text(current_network),
               hbox({
                   wifi_toggle_->Render(),
                   scan_button->Render(),
               }),
               window(text("Network"),
                      menu_scan->Render() | vscroll_indicator | yframe),
           }) |
           flex;
  }

  Element RenderConnect() {
    return vbox({
               vbox({
                   hbox({text("Network : "), text(wifi_list_[selected])}),
                   hbox({text("Password: "), pass_input->Render()}),
               }),
               hbox({
                   connect_button->Render(),
                   disconnect_button->Render(),
                   back_button->Render(),
               }),
           }) |
           nothing;
  }

 private:
  void Scan() {
    if (wifi_scan_.joinable())
      wifi_scan_.join();

    scanning_ = true;
    wifi_scan_ = std::thread([=] {
      ListWifiNames(wifi_list_receiver_->MakeSender(), [=] {
        scanning_ = false;
        screen_->PostEvent(Event::Custom);
      });
    });
  }

  void Connect() {
    data.name = wifi_list_[selected];
    data.pass = password;
    data.type = "wifi";
    std::string file_path;
    RefreshNetworkList();
    for (auto n : service_names) {
      if (n.first == data.name) {
        file_path = CONNMAN_SERVICE_F_PATH + n.second + ".config";
        lookup_table[n.second][SERVICE_KEY_TYPE] = data.type;
        lookup_table[n.second][SERVICE_KEY_NAME] = data.name;
        lookup_table[n.second][SERVICE_KEY_PASSPHRASE] = data.pass;
        break;
      }
    }
    if (file_path.empty()) {
      return;
    }
    StoreConnmanFile(file_path.c_str());
    activity = ActivityMain;
  }

  void Disconnect() {
    data.name = wifi_list_[selected];
    data.pass = password;
    data.type = "wifi";
    std::string file_path;
    RefreshNetworkList();
    for (auto n : service_names) {
      if (n.first == data.name) {
        file_path = CONNMAN_SERVICE_F_PATH + n.second + ".config";
        lookup_table[n.second][SERVICE_KEY_TYPE] = data.type;
        lookup_table[n.second][SERVICE_KEY_NAME] = data.name;
        lookup_table[n.second][SERVICE_KEY_PASSPHRASE] = data.pass;
        break;
      }
    }
    EmptyConnmanConfig(file_path.c_str());
    activity = ActivityMain;
  }

  void ToggleWifi() {
    if (!WifiStatus())
      shell_helper("connmanctl enable wifi");
    else
      shell_helper("connmanctl disable wifi");
  }

  void ListWifiNames(Sender<std::vector<std::string>> out,
                     std::function<void(void)> done) {
    std::vector<std::string> list;
    RefreshNetworkList();
    for (auto network : service_names)
      list.push_back(network.first);
    out->Send(list);
    done();
  }

  int StoreConnmanFile(const char* path) {
    std::fstream connman_config(
        path, std::fstream::in | std::fstream::out | std::fstream::trunc);
    if (!connman_config.is_open()) {
      return -1;
    } else {
      std::time_t time_ = std::chrono::system_clock::to_time_t(
          std::chrono::system_clock::now());
      auto ti = std::ctime(&time_);
      connman_config << "# Edited by beagle config: " << ti << "\n";
      for (auto sname : lookup_table) {
        if (sname.second[SERVICE_KEY_NAME] == data.name) {
          connman_config << "[service_" << sname.first << "]"
                         << "\n";
          for (auto name_value : sname.second) {
            connman_config << name_value.first << " = " << name_value.second
                           << "\n";
          }
          connman_config << "\n";
        }
      }
    }
    return 0;
  }

  void EmptyConnmanConfig(const char* path) {
    std::fstream connman_config(
        path, std::fstream::in | std::fstream::out | std::fstream::trunc);
    if (!connman_config.is_open()) {
    } else {
      std::time_t time_ = std::chrono::system_clock::to_time_t(
          std::chrono::system_clock::now());
      auto ti = std::ctime(&time_);
      connman_config << "# Edited by beagle config: " << ti << "\n";
      for (auto sname : lookup_table) {
        if (sname.second[SERVICE_KEY_NAME] == data.name) {
          connman_config << "#[service_" << sname.first << "]"
                         << "\n";
          for (auto name_value : sname.second) {
            connman_config << "#" << name_value.first << " = "
                           << name_value.second << "\n";
          }
          connman_config << "\n";
        }
      }
    }
  }

  /*
    Get wifi SSIDs and unique names
    1. Enables wifi
    2. Scans
    3. Parses the output
  */
  void RefreshNetworkList() {
    if (!WifiStatus()) {
      /* Enable wifi */
      shell_helper("connmanctl enable wifi");
    }

    /* Scan wifi */
    shell_helper("connmanctl scan wifi");

    /* Get mac address formatted into continuous string */
    shell_helper(
        "cat /sys/class/net/wlan0/address | sed  -r "
        "'s/^([^:]{2}):([^:]{2}):([^:]{2}):([^:]{2}):([^:]{2}):([^:]{2})$/"
        "\\1\\2\\3\\4\\5\\6/'",
        &result);

    /* Get macaddress from trailing spaces */
    auto mac_address = result.substr(0, result.length() - 2);

    /* Get connman services */
    shell_helper("connmanctl services | sed -e 's/[ \t]*//'", &result);

    /* Temporary string to store wifi_macaddress*/
    auto temp_start = "wifi_" + mac_address;

    while (true) {
      if (result.length() == 0) {
        break;
      }

      /* Position of newline character */
      size_t newline_pos = 1;
      if (result.find("\n") != std::string::npos) {
        newline_pos = result.find("\n");

        /* Get current line from trimmed sub sequence */
        auto current_line = reduce(result.substr(0, newline_pos));

        /* Position of wifi_<macaddress> */
        auto pos = current_line.find(temp_start);

        /* Default check for std::string::npos */
        if (pos != std::string::npos) {
          /* pos==0 means that it is hidden and has no name*/
          if (pos != 0) {
            auto name = reduce(current_line.substr(0, pos));
            auto act_pos = name.find("*A");
            if (act_pos != std::string::npos) {
              name.erase(act_pos, act_pos + name.find_first_of(" ") + 1);
              active_name = name;
            }
            auto unique_name = current_line.substr(pos);
            service_names[name] = unique_name;
          } else {
            service_names["hidden - " + current_line.substr(0, 10)] =
                current_line;
          }
        } else {
        }
      }
      result.erase(0, newline_pos + 1);
    }
  }

  std::string ActiveWifiName() {
    int sockfd;
    char active_wlan_id[IW_ESSID_MAX_SIZE + 1];

    struct iwreq wlan_ioctl_req;
    memset(&wlan_ioctl_req, 0, sizeof(struct iwreq));
    wlan_ioctl_req.u.essid.length = IW_ESSID_MAX_SIZE + 1;

    sprintf(wlan_ioctl_req.ifr_name, "wlan0");

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
      return active_name = std::string("Error: ") + *strerror(errno);
    }

    wlan_ioctl_req.u.essid.pointer = active_wlan_id;
    if (ioctl(sockfd, SIOCGIWESSID, &wlan_ioctl_req) == -1) {
      return active_name = std::string("Error: ") + *strerror(errno);
    }
    close(sockfd);
    active_name = (char*)wlan_ioctl_req.u.essid.pointer;
    if (active_name.length())
      return active_name;
    return active_name = "None";
  }

  bool WifiStatus() {
    int skfd = socket(AF_INET, SOCK_DGRAM, 0);

    if (skfd < 0) {
      return 0;
    }

    const char* ifname = "wlan0";
    struct ifreq req;

    strncpy(req.ifr_ifrn.ifrn_name, ifname, IFNAMSIZ);

    int err = ioctl(skfd, SIOCGIFFLAGS, &req);
    if (err) {
      perror("SIOCGIFFLAGS");
      return err;
    } else {
      int flags = req.ifr_ifru.ifru_flags;
      return (flags & IFF_UP) ? true : false;
    }
    return 0;
  }

  int selected = 0;
  enum Activity : int {
    ActivityMain,
    ActivityConnect,
  };
  int activity = ActivityMain;
  std::string selected_wifi;
  std::string password;
  std::vector<std::string> names;
  std::thread wifi_scan_;
  connman_data data;
  Component scan_button;
  Component menu_scan;
  MenuOption option;
  Component pass_input;
  Component connect_button;
  Component disconnect_button;
  Component back_button;
  Component wifi_toggle_;
  std::vector<std::string> wifi_list_;
  bool scanning_ = false;
  bool wifiCompatiblity = true;
  std::string scan_label_;
  Receiver<decltype(wifi_list_)> wifi_list_receiver_ =
      MakeReceiver<decltype(wifi_list_)>();
  ScreenInteractive* screen_;
  /* Command Result */
  std::string result;

  /* Current Wifi name */
  std::string service_name;
  std::string active_name;

  /*
   * List of normal - unique names from connmanctl
   * John ----- wifi_<macaddress>_<hash>_managed_psk
   */
  std::unordered_map<std::string, std::string> service_names;

  /* Service name and its key-value */
  std::unordered_map<std::string, std::unordered_map<std::string, std::string>>
      lookup_table;
};

struct Network {
  /* wifi | ethernet */
  std::string name;

  /* D-Bus Path */
  std::string path;

  /* Operational State */
  std::string OperationalState;

  /* Carrier State */
  std::string CarrierState;

  /* Address State */
  std::string AddressState;

  /* Administrative State */
  std::string AdministrativeState;
};

class WpaImpl : public PanelBase {
 public:
  WpaImpl(ScreenInteractive* screen) : screen_(screen) {
    /* Build UI */
    password_option_.password = true;
    ssid_input_ = Input(&ssid, "ssid");
    psk_input_ = Input(&psk, "psk", password_option_);

    load_net_info();
    get_wlan0_properties();

    Component page = Container::Tab(
        {
            Container::Vertical({}),
            Container::Horizontal({
                refresh_button_,
                add_button_,
            }),
            Container::Vertical({
                ssid_input_,
                psk_input_,
                Container::Horizontal({
                    back_button_,
                    connect_button_,
                }),
            }),
            Container::Vertical({}),
        },
        &tabSelected_);

    Add(page);
  }

  ~WpaImpl() {
    if (thread_.joinable())
      thread_.join();
  };

  std::string Title() override { return "WiFi"; }

 private:
  // Read DBUS wlan status
  void get_wlan0_properties() {
    GDBusProxy* props_proxy;
    GError* error = NULL;
    GVariant *ret = NULL, *value = NULL;
    const char* name = NULL;

    props_proxy = g_dbus_proxy_new_for_bus_sync(
        G_BUS_TYPE_SYSTEM, G_DBUS_PROXY_FLAGS_NONE, NULL,
        "org.freedesktop.network1", wlan_.path.c_str(),
        "org.freedesktop.DBus.Properties", NULL, &error);

    /* If proxy not found display error */
    if (!props_proxy) {
      tabSelected_ = 0;
      return;
    }

    g_assert(props_proxy);

    ret = g_dbus_proxy_call_sync(
        props_proxy, "GetAll",
        g_variant_new("(s)", "org.freedesktop.network1.Link"),
        G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);

    /* If return not found display error */
    if (!ret) {
      tabSelected_ = 0;
      return;
    }

    g_variant_get(ret, "(@a{sv})", &value);

    g_variant_lookup(value, "OperationalState", "&s", &name);
    wlan_.OperationalState = name;
    g_variant_lookup(value, "AddressState", "&s", &name);
    wlan_.AddressState = name;
    g_variant_lookup(value, "CarrierState", "&s", &name);
    wlan_.CarrierState = name;
    g_variant_lookup(value, "AdministrativeState", "&s", &name);
    wlan_.AdministrativeState = name;

    if (ret)
      g_variant_unref(ret);
    g_object_unref(props_proxy);
  }

  // Read DBUS systemd-networkd info
  void load_net_info() {
    GDBusProxy* props_proxy;
    GError* error = NULL;
    GVariant* ret = NULL;
    GVariantIter* iter;
    char *path = NULL, *name = NULL;
    gint32 num;

    /* Create a D-Bus proxy for systemd-networkd */
    props_proxy = g_dbus_proxy_new_for_bus_sync(
        G_BUS_TYPE_SYSTEM, G_DBUS_PROXY_FLAGS_NONE, NULL,
        "org.freedesktop.network1", "/org/freedesktop/network1",
        "org.freedesktop.network1.Manager", NULL, &error);

    /* If proxy not found display error */
    if (!props_proxy) {
      tabSelected_ = 0;
      return;
    }

    g_assert(props_proxy);

    ret = g_dbus_proxy_call_sync(props_proxy, "ListLinks", NULL,
                                 G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);

    /* If nothing return display error */
    if (!ret) {
      tabSelected_ = 0;
      return;
    }

    /* Get all the device support for systemd-networkd */
    g_variant_get(ret, "(a(iso))", &iter);
    while (g_variant_iter_loop(iter, "(iso)", &num, &name, &path)) {
      std::string str_name(name);
      std::string str_path(path);
      /* Store the variable into network struct */
      if (!str_name.compare("wlan0")) {
        wlan_.name = str_name;
        wlan_.path = str_path;
      }
    }

    if (props_proxy)
      g_object_unref(props_proxy);
    if (ret)
      g_variant_unref(ret);
  }

  // Connect WiFi through updating the .config file
  void connectWiFi() {
    const std::string wpa_path =
        "/etc/wpa_supplicant/wpa_supplicant-wlan0.conf";
    const std::string temp_path = "temp.bin";

    std::ifstream infile(wpa_path);
    std::ofstream outfile(temp_path);

    if (!infile) {
      std::cout << "No file found\n";
    }

    std::string line;

    while (std::getline(infile, line)) {
      if (!line.compare("network={")) {
        outfile << line << std::endl;
        outfile << "\tssid=\"" << ssid << "\"" << std::endl;
        outfile << "\tpsk=\"" << psk << "\"" << std::endl;
        outfile << "}" << std::endl;
        break;
      }
      outfile << line << std::endl;
    }

    infile.close();
    outfile.close();

    // Waiting for reconfigure
    if (thread_.joinable())
      thread_.join();

    // Update the file
    std::rename(temp_path.c_str(), wpa_path.c_str());

    // Go to waiting page
    tabSelected_ = 3;

    // Restart the service for reconfigure
    restart_wpa_service();
    restart_wpa_wlan0_service();

    thread_ = std::thread([=] {
      // Wait for the services to restart
      // Not sure how to handle interupt signal
      sleep(6);
      tabSelected_ = 1;
      get_wlan0_properties();
      screen_->PostEvent(Event::Custom);
    });
  }

  // Restart the services to reconfigure the network
  void restart_wpa_wlan0_service() {
    GError* error = NULL;
    GVariant* ret;
    GDBusProxy* proxy;

    /* Create a D-Bus proxy for systemd */
    proxy = g_dbus_proxy_new_for_bus_sync(
        G_BUS_TYPE_SYSTEM, G_DBUS_PROXY_FLAGS_NONE, NULL,
        "org.freedesktop.systemd1", "/org/freedesktop/systemd1",
        "org.freedesktop.systemd1.Manager", NULL, NULL);

    /* If proxy not found display error */
    if (!props_proxy) {
      tabSelected_ = 0;
      return;
    }

    g_assert(proxy);

    /* Call RestartUnit function to restart the service */
    ret = g_dbus_proxy_call_sync(
        proxy, "RestartUnit",
        g_variant_new("(ss)", "wpa_supplicant@wlan0.service", "replace"),
        G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);

    /* Error Handling */
    if (!ret) {
      tabSelected_ = 0;
      return;
    } else {
      g_variant_unref(ret);
    }

    /* free the memory after used */
    if (proxy)
      g_object_unref(proxy);
  }

  // Restart the services to reconfigure the network
  void restart_wpa_service() {
    GError* error = NULL;
    GVariant* ret;
    GDBusProxy* proxy;

    /* Create a D-Bus proxy for systemd */
    proxy = g_dbus_proxy_new_for_bus_sync(
        G_BUS_TYPE_SYSTEM, G_DBUS_PROXY_FLAGS_NONE, NULL,
        "org.freedesktop.systemd1", "/org/freedesktop/systemd1",
        "org.freedesktop.systemd1.Manager", NULL, NULL);

    /* If proxy not found display error */
    if (!props_proxy) {
      tabSelected_ = 0;
      return;
    }
    
    g_assert(proxy);

    /* Call RestartUnit function to restart the service */
    ret = g_dbus_proxy_call_sync(
        proxy, "RestartUnit",
        g_variant_new("(ss)", "wpa_supplicant.service", "replace"),
        G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);

    /* Error Handling */
    if (!ret) {
      tabSelected_ = 0;
      return;
    } else {
      g_variant_unref(ret);
    }

    /* free the memory after used */
    if (proxy)
      g_object_unref(proxy);
  }

  // Return ColorCode for WiFi status
  ftxui::Color colorCode(const std::string& str) {
    if (!str.compare("routable")) {
      return Color::Green;
    } else if (!str.compare("configured")) {
      return Color::Green;
    } else if (!str.compare("configuring")) {
      return Color::Yellow;
    } else if (!str.compare("dormant")) {
      return Color::Red;
    } else {
      return Color::Default;
    }
  }

  Element Render() override {
    Element content = vbox({text("TYPE : " + wlan_.name),
                            hbox({
                                text("OPERATIONAL : "),
                                text(wlan_.OperationalState) |
                                    color(colorCode(wlan_.OperationalState)),
                            }),
                            hbox({
                                text("SETUP       : "),
                                text(wlan_.AdministrativeState) |
                                    color(colorCode(wlan_.AdministrativeState)),
                            }),
                            hbox({
                                refresh_button_->Render(),
                                add_button_->Render(),
                            })});

    Element error = vbox({
        text(" "),
        text("Sorry Features not supported!") | bold | color(Color::Red),
    });

    Element add =
        vbox({text("[!!] Requires sudo or root permision.") | color(Color::Red),
              text("Connect WiFi") | bold,
              hbox({
                  text("SSID : "),
                  ssid_input_->Render(),
              }),
              hbox({
                  text("PASSWORD : "),
                  psk_input_->Render(),
              }),
              hbox({
                  back_button_->Render(),
                  connect_button_->Render(),
              })});

    Element waiting = window(text(""), text("Configuring WiFi ...")) | center |
                      size(HEIGHT, GREATER_THAN, 10);

    switch (tabSelected_) {
      case 0:
        return error;
      case 1:
        return content;
      case 2:
        return add;
      case 3:
        return waiting;
      default:
        return content;
    }
  }

  int tabSelected_ = 1;
  std::string ssid;
  std::string psk;
  Network wlan_;
  std::vector<std::string> tabValues_;
  std::thread thread_;
  ScreenInteractive* screen_;
  InputOption password_option_;
  MenuOption option = MenuOption::HorizontalAnimated();
  Component tab_ = Container::Tab({}, &tabSelected_);
  Component add_button_ =
      Button("Connect Network", [this] { tabSelected_ = 2; });
  Component refresh_button_ =
      Button("Refresh", [this] { get_wlan0_properties(); });
  Component ssid_input_;
  Component psk_input_;
  Component back_button_ = Button("Back", [this] { tabSelected_ = 1; });
  Component connect_button_ = Button("Connect", [this] { connectWiFi(); });
};

}  // namespace

namespace panel {
Panel WiFi(ScreenInteractive* screen) {
  // File path for device's OS info
  const std::string deviceInfo = "/etc/os-release";

  std::string line;
  std::ifstream infile(deviceInfo);

  // Error handle if file not found
  if (!infile) {
    return Make<WiFiImpl>(screen);
  }

  // Read 1st line of the file
  getline(infile, line);
  std::string header, info;
  std::stringstream ss(line);
  getline(ss, header, '=');
  getline(ss, info, '=');

  // Debian 11 navigate to WPA method
  if (!info.compare("\"Debian GNU/Linux 11 (bullseye)\""))
    return Make<WpaImpl>(screen);

  // Version below Debian 11
  else
    return Make<WiFiImpl>(screen);
}
}  // namespace panel

}  // namespace ui
